package transxchange

import "encoding/xml"

type VehicleJourney struct {
	XMLName xml.Name `xml:"VehicleJourney"`

	SequenceNumber       string `xml:"SequenceNumber,attr"`
	CreationDateTime     string `xml:"CreationDateTime,attr"`
	ModificationDateTime string `xml:"ModificationDateTime,attr"`
	Modification         string `xml:"Modification,attr"`
	RevisionNumber       string `xml:"RevisionNumber,attr"`
	PrivateCode          string `xml:"PrivateCode"`
	OperatorRef          string `xml:"OperatorRef"`
	Direction            string `xml:"Direction"`
	Operational          struct {
		TicketMachine struct {
			JourneyCode string `xml:"JourneyCode"`
		} `xml:"TicketMachine"`
	} `xml:"Operational"`
	OperatingProfile struct {
		RegularDayType struct {
			DaysOfWeek struct {
				Monday    string `xml:"Monday"`
				Tuesday   string `xml:"Tuesday"`
				Wednesday string `xml:"Wednesday"`
				Thursday  string `xml:"Thursday"`
				Friday    string `xml:"Friday"`
				Saturday  string `xml:"Saturday"`
			} `xml:"DaysOfWeek"`
		} `xml:"RegularDayType"`
		BankHolidayOperation struct {
			DaysOfOperation struct {
				GoodFriday string `xml:"GoodFriday"`
			} `xml:"DaysOfOperation"`
			DaysOfNonOperation struct {
				ChristmasDay                     string `xml:"ChristmasDay"`
				BoxingDay                        string `xml:"BoxingDay"`
				NewYearsDay                      string `xml:"NewYearsDay"`
				LateSummerBankHolidayNotScotland string `xml:"LateSummerBankHolidayNotScotland"`
				MayDay                           string `xml:"MayDay"`
				EasterMonday                     string `xml:"EasterMonday"`
				SpringBank                       string `xml:"SpringBank"`
				ChristmasDayHoliday              string `xml:"ChristmasDayHoliday"`
				BoxingDayHoliday                 string `xml:"BoxingDayHoliday"`
				NewYearsDayHoliday               string `xml:"NewYearsDayHoliday"`
				ChristmasEve                     string `xml:"ChristmasEve"`
				NewYearsEve                      string `xml:"NewYearsEve"`
				GoodFriday                       string `xml:"GoodFriday"`
			} `xml:"DaysOfNonOperation"`
		} `xml:"BankHolidayOperation"`
	} `xml:"OperatingProfile"`
	GarageRef                string                     `xml:"GarageRef"`
	VehicleJourneyCode       string                     `xml:"VehicleJourneyCode"`
	ServiceRef               string                     `xml:"ServiceRef"`
	LineRef                  string                     `xml:"LineRef"`
	JourneyPatternRef        string                     `xml:"JourneyPatternRef"`
	DepartureTime            string                     `xml:"DepartureTime"`
	VehicleJourneyTimingLink []VehicleJourneyTimingLink `xml:"VehicleJourneyTimingLink"`
}

type VehicleJourneyTimingLink struct {
	XMLName xml.Name `xml:"VehicleJourneyTimingLink"`

	ID                          string `xml:"id,attr"`
	JourneyPatternTimingLinkRef string `xml:"JourneyPatternTimingLinkRef"`
	RunTime                     string `xml:"RunTime"`
	From                        struct {
		WaitTime string `xml:"WaitTime"`
	} `xml:"From"`
}
