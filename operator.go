package transxchange

import "encoding/xml"

type Operator struct {
	XMLName xml.Name `xml:"Operator"`

	ID                    string   `xml:"id,attr"`
	CreationDateTime      string   `xml:"CreationDateTime,attr"`
	ModificationDateTime  string   `xml:"ModificationDateTime,attr"`
	Modification          string   `xml:"Modification,attr"`
	RevisionNumber        string   `xml:"RevisionNumber,attr"`
	NationalOperatorCode  string   `xml:"NationalOperatorCode"`
	OperatorCode          string   `xml:"OperatorCode"`
	OperatorShortName     string   `xml:"OperatorShortName"`
	OperatorNameOnLicence string   `xml:"OperatorNameOnLicence"`
	TradingName           string   `xml:"TradingName"`
	LicenceNumber         string   `xml:"LicenceNumber"`
	Garages               []Garage `xml:"Garages>Garage"`
}

type Garage struct {
	XMLName xml.Name `xml:"Garage"`

	Code     string   `xml:"GarageCode"`
	Name     string   `xml:"GarageName"`
	Location Location `xml:"Location"`
}
