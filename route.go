package transxchange

import "encoding/xml"

type Route struct {
	XMLName xml.Name `xml:"Route"`

	ID                   string `xml:"id,attr"`
	CreationDateTime     string `xml:"CreationDateTime,attr"`
	ModificationDateTime string `xml:"ModificationDateTime,attr"`
	Modification         string `xml:"Modification,attr"`
	RevisionNumber       string `xml:"RevisionNumber,attr"`
	PrivateCode          string `xml:"PrivateCode"`
	Description          string `xml:"Description"`
	RouteSectionRef      string `xml:"RouteSectionRef"`
}
