package transxchange

import "encoding/xml"

type AnnotatedStopPointRef struct {
	XMLName xml.Name `xml:"AnnotatedStopPointRef"`

	StopPointRef string `xml:"StopPointRef"`
	CommonName   string `xml:"CommonName"`
}
