package transxchange

import "encoding/xml"

type RouteSection struct {
	XMLName xml.Name `xml:"RouteSection"`

	ID string `xml:"id,attr"`

	RouteLinks []RouteLink `xml:"RouteLink"`
}
