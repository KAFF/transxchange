package transxchange

import "encoding/xml"

type RouteLink struct {
	XMLName xml.Name `xml:"RouteLink"`

	ID                   string     `xml:"id,attr"`
	CreationDateTime     string     `xml:"CreationDateTime,attr"`
	ModificationDateTime string     `xml:"ModificationDateTime,attr"`
	Modification         string     `xml:"Modification,attr"`
	RevisionNumber       string     `xml:"RevisionNumber,attr"`
	From                 string     `xml:"From>StopPointRef"`
	To                   string     `xml:"To>StopPointRef"`
	Distance             string     `xml:"Distance"`
	Track                []Location `xml:"Track>Mapping>Location"`
}

type Location struct {
	XMLName xml.Name `xml:"Location"`

	ID        string `xml:"id,attr"`
	Longitude string `xml:"Longitude"`
	Latitude  string `xml:"Latitude"`
}
