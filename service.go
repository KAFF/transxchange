package transxchange

import "encoding/xml"

type Service struct {
	XMLName xml.Name `xml:"Service"`

	CreationDateTime         string          `xml:"CreationDateTime,attr"`
	ModificationDateTime     string          `xml:"ModificationDateTime,attr"`
	Modification             string          `xml:"Modification,attr"`
	RevisionNumber           string          `xml:"RevisionNumber,attr"`
	ServiceCode              string          `xml:"ServiceCode"`
	Lines                    []Line          `xml:"Lines>Line"`
	OperatingPeriod          OperatingPeriod `xml:"OperatingPeriod"`
	TicketMachineServiceCode string          `xml:"TicketMachineServiceCode"`
	RegisteredOperatorRef    string          `xml:"RegisteredOperatorRef"`
	PublicUse                string          `xml:"PublicUse"`
	StandardService          StandardService `xml:"StandardService"`
}

type Line struct {
	XMLName             xml.Name        `xml:"Line"`
	ID                  string          `xml:"id,attr"`
	LineName            string          `xml:"LineName"`
	OutboundDescription LineDescription `xml:"OutboundDescription"`
	InboundDescription  LineDescription `xml:"InboundDescription"`
}

type LineDescription struct {
	Origin      string `xml:"Origin"`
	Destination string `xml:"Destination"`
	Description string `xml:"Description"`
}

type OperatingPeriod struct {
	XMLName xml.Name `xml:"OperatingPeriod"`

	StartDate string `xml:"StartDate"`
	EndDate   string `xml:"EndDate"`
}

type StandardService struct {
	XMLName xml.Name `xml:"StandardService"`

	Origin           string           `xml:"Origin"`
	Destination      string           `xml:"Destination"`
	UseAllStopPoints string           `xml:"UseAllStopPoints"`
	JourneyPatterns  []JourneyPattern `xml:"JourneyPattern"`
}

type JourneyPattern struct {
	XMLName xml.Name `xml:"JourneyPattern"`

	ID                        string `xml:"id,attr"`
	CreationDateTime          string `xml:"CreationDateTime,attr"`
	ModificationDateTime      string `xml:"ModificationDateTime,attr"`
	Modification              string `xml:"Modification,attr"`
	RevisionNumber            string `xml:"RevisionNumber,attr"`
	DestinationDisplay        string `xml:"DestinationDisplay"`
	OperatorRef               string `xml:"OperatorRef"`
	Direction                 string `xml:"Direction"`
	RouteRef                  string `xml:"RouteRef"`
	JourneyPatternSectionRefs string `xml:"JourneyPatternSectionRefs"`
}
