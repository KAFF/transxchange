package transxchange

import "encoding/xml"

type JourneyPatternSection struct {
	XMLName xml.Name `xml:"JourneyPatternSection"`

	ID                        string                     `xml:"id,attr"`
	JourneyPatternTimingLinks []JourneyPatternTimingLink `xml:"JourneyPatternTimingLink"`
}

type JourneyPatternTimingLink struct {
	XMLName xml.Name `xml:"JourneyPatternTimingLink"`

	ID           string `xml:"id,attr"`
	From         From   `xml:"From"`
	To           To     `xml:"To"`
	RouteLinkRef string `xml:"RouteLinkRef"`
	RunTime      string `xml:"RunTime"`
}

type From struct {
	XMLName xml.Name `xml:"From"`

	SequenceNumber            string `xml:"SequenceNumber,attr"`
	ID                        string `xml:"id,attr"`
	Activity                  string `xml:"Activity"`
	DynamicDestinationDisplay string `xml:"DynamicDestinationDisplay"`
	StopPointRef              string `xml:"StopPointRef"`
	TimingStatus              string `xml:"TimingStatus"`
	FareStageNumber           string `xml:"FareStageNumber"`
	WaitTime                  string `xml:"WaitTime"`
}
type To struct {
	XMLName xml.Name `xml:"To"`

	SequenceNumber            string `xml:"SequenceNumber,attr"`
	ID                        string `xml:"id,attr"`
	Activity                  string `xml:"Activity"`
	DynamicDestinationDisplay string `xml:"DynamicDestinationDisplay"`
	StopPointRef              string `xml:"StopPointRef"`
	TimingStatus              string `xml:"TimingStatus"`
	FareStageNumber           string `xml:"FareStageNumber"`
}
