package transxchange

import "encoding/xml"

type TransXChange struct {
	XMLName xml.Name `xml:"TransXChange"`

	Xsi                    string                  `xml:"xsi,attr"`
	Xmlns                  string                  `xml:"xmlns,attr"`
	CreationDateTime       string                  `xml:"CreationDateTime,attr"`
	FileName               string                  `xml:"FileName,attr"`
	Modification           string                  `xml:"Modification,attr"`
	ModificationDateTime   string                  `xml:"ModificationDateTime,attr"`
	RegistrationDocument   string                  `xml:"RegistrationDocument,attr"`
	RevisionNumber         string                  `xml:"RevisionNumber,attr"`
	SchemaVersion          string                  `xml:"SchemaVersion,attr"`
	SchemaLocation         string                  `xml:"schemaLocation,attr"`
	StopPoints             []AnnotatedStopPointRef `xml:"StopPoints>AnnotatedStopPointRef"`
	RouteSections          []RouteSection          `xml:"RouteSections>RouteSection"`
	Routes                 []Route                 `xml:"Routes>Route"`
	JourneyPatternSections []JourneyPatternSection `xml:"JourneyPatternSections>JourneyPatternSection"`
	Operators              []Operator              `xml:"Operators>Operator"`
	Services               []Service               `xml:"Services>Service"`
	VehicleJourneys        []VehicleJourney        `xml:"VehicleJourneys>VehicleJourney"`
}
